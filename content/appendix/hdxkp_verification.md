---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.12
    jupytext_version: 1.9.1
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell} ipython3
:tags: [remove-input]

import os
import sys
sys.path.insert(0, os.path.abspath('../../lib'))

import plantumlmagic
```

(appendix:hdxkp_verification)=
# Verification of Health Department Keypair Certificates

## Certificate Chain and Trust Anchor

```{code-cell} ipython3
:tags: [remove-input]

%%plantuml

@startuml
frame "Health Department Certificate Chain" {

card HDCC_Root [
CN: D-TRUST Limited Basic Root CA 1 2019
----
Validity: 19.06.2019 - 19.06.2034
CA: TRUE
Key Usage: Certificate Sign
----
<i>Trust Anchor: Pinned in the Client</i>
]

card HDCC_Intermediate [
CN: D-TRUST Limited Basic CA 1-2 2019
----
Validity: 20.08.2019 - 19.06.2034
Serial Number: 0FE54A
OCSP: limited-basic-root-ca-1-2019.ocsp.d-trust.net
CA: TRUE
Key Usage: Certificate Sign
----
Issuer: D-TRUST Limited Basic Root CA 1 2019
]

card HDCC_Leaf [
CN: x.xx.x.xx.luca, OU: "LUCA"
----
Validity: xx.xx.2021 - xx.xx.2024
Serial Number: 0815
OCSP: d-trust-limited-basic-ca-1-2-2019.ocsp.d-trust.net
key usages: Digital Signature, TLS Client Authentication
----
Issuer: D-TRUST Limited Basic CA 1-2 2019
]

HDCC_Root --> HDCC_Intermediate : issues
HDCC_Intermediate --> HDCC_Leaf : issues

}

frame "Health Department Application Certificates" {

card HDSKP [
Health Department Signing Key Pair
----
sub: "0e162322-04ac-4609-bd8c-48e2cd925843"
name: "Gesundheitsamt Beispielfurt"
type: "HDSKP"
]

card HDEKP [
Health Department Encryption Key Pair
----
sub: "0e162322-04ac-4609-bd8c-48e2cd925843"
name: "Gesundheitsamt Beispielfurt"
type: "HDEKP"
]

}

HDCC_Leaf --> HDSKP : signs
HDCC_Leaf --> HDEKP : signs
@enduml
```

## Technical Implementation

### Health Department Certificate Chain

The Health Department Certificate Chain is implemented as an ordinary X.509 certificate chain managed by a {term}`Trusted 3rd Party`.
The CA certificates contain 2048bit RSA public keys, the {term}`Health Department`-specific certificates provides a 4096bit RSA public key.
Furthermore, the {term}`Health Department`-specific certificate claims "LUCA" as "Organizational Unit" in their distinguished name along with the specific health department's identification as a FHIR code [^fhir].

[^fhir]: FHIR - Fast Healthcare Interoperability Resources

### Health Department Key Pairs (HDSKP/HDEKP)

The _luca_-specific {term}`Health Department` key pairs (a.k.a {term}`HDSKP` and {term}`HDEKP`) are signed by the respective {term}`Health Department Certificate` and are implemented as Json Web Tokens.
Along with an EC P-256 public key they contain meta data about the owning {term}`Health Department`, namely:

 * UUID of the Health Department in the _luca_ system
 * SHA-1 of the issuing Health Department Certificate
 * Common name of the Health Department
 * Type of key (either HDSKP or HDEKP)
 * issue date

## Trust Boundaries and Assumptions

### Trusted 3rd Party

The entire {term}`Health Department Certificate` chain is provided by a {term}`Trusted 3rd Party` (namely: [D-Trust](https://www.d-trust.net) which is a subsidiary of the [Bundesdruckerei](https://www.bundesdruckerei.de)) and not under the control of the {term}`Luca Service Operator`.
Therefore, the entire _luca_ system (and its users) must rely on the integrity of the {term}`Trusted 3rd Party`.
_Luca_ particularly relies on the {term}`Trusted 3rd Party` to not issue valid {term}`Health Department Certificate`s to unauthorized individuals and to provide an effective way to revoke certificates if necessary.
The root certificate "D-TRUST Limited Basic Root CA 1 2019" is the trust anchor for the application-specific trust chains in _luca_.

### Health Department Administrator

Furthermore, _luca_ relies on the integrity of the individual {term}`Health Department` administrators.
In particular that they issue a certified {term}`HDSKP` and {term}`HDEKP` for their {term}`Health Department` using their {term}`Health Department Certificate`'s private key.
During registration, the {term}`Luca Server` verifies the that the  administrator issues the {term}`HDSKP`/{term}`HDEKP` for the {term}`Health Department` that they presented a valid {term}`Health Department Certificate` for.
If necessary, the {term}`Trusted 3rd Party` can revoke specific {term}`Health Department Certificate`s invalidating administrator-issued {term}`HDSKP`/{term}`HDEKP`.

### Luca

All components in _luca_ trust the authenticity and integrity of public key material and meta information contained in a successfully verified {term}`HDSKP` or {term}`HDEKP`.
This allows all actors in the system to securely communicate with {term}`Health Department`s.
Given the defined trust assumptions above, the authenticity of {term}`HDSKP` and {term}`HDEKP` is not dependent on the integrity of the {term}`Luca Server`.

## Certificate Verification

### Verification of Health Department Certificate Chain

The {term}`Luca Server` provides the necessary certificates for the full certificate chain validation (namely specific {term}`Health Department Certificate`s, intermediate CA certificate along with recent OCSP responses).
User facing applications (e.g. {term}`Guest App`s, {term}`Health Department Frontend`, {term}`Venue Owner Frontend`) are shipped with the trust anchor mentioned above (hard-coded).
With this information, an ordinary X.509 certificate chain validation is performed using standard implementations depending on the specific platform.

In particular but not limited to, the following checks are performed:

 * Validity time interval (against the local time)
 * Issuer DN and signature
 * Issuer key usages and CA status
 * For Intermediate CA and Leaf Certificates
    * Revocation status (based on OCSP stapling)
        * Creation time stamp is recent enough
        * Signature chain is rooted in the trust anchor
        * Respective certificate serial number is marked as "good" (i.e. not "revoked")
 * For Leaf certificates (Health Department Certificates)
    * OU is "LUCA"
    * CN is "\<FHIR-code\>.luca"
    * Key Usage contains "Digital Signature"

If all verification checks are successful the {term}`Health Department Certificate` is considered trustworthy.
Hence, the _luca_ application will assume that the certificate is indeed owned by a legitimate {term}`Health Department`.

### Verification of HDSKP/HDEKP

Both {term}`Health Department` key pairs ({term}`HDSKP`, {term}`HDEKP`) were {ref}`signed by the Health Department Certificate <process:health_department_registration:HDxKP>` during the registration of the {term}`Health Department`.
Hence, _luca_ client applications consider {term}`HDSKP`/{term}`HDEKP` JWTs valid if their signature matches a {term}`Health Department Certificate` that was successfully verified with the process described above.

As a result, the application considers application-specific messages and assets signed by the {term}`HDSKP` authentic and originating from the associated legitimate {term}`Health Department` (e.g. see {ref}`process:daily_key`).
Similarly, the application will consider the public key contained in {term}`HDEKP` authentic and use it for encrypting messages addressed to the mentioned {term}`Health Department`.

Note that _luca_ currently does not implement a revocation mechanism for {term}`HDSKP`/{term}`HDEKP` certificates.
If one of these keys is compromised, the {term}`Health Department Certificate` will need to be revoked via the {term}`Trusted 3rd Party`.
