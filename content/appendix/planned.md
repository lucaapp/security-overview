(appendix:planned)=
# Planned Improvements

_luca_ is under continuous development.
This appendix collects improvements we are currently working on.


## Signing of Contact Data Requests

Currently, requests by the {term}`Health Department` addressed to {term}`Venue Owner`s to provide access to their {term}`Guest`'s {term}`Contact Data` are not signed by the issuing {term}`Health Department`.
As of August 2021 {term}`Health Department`s have access to a {term}`HDSKP` certificate that is based on the trust anchor of a {term}`Trusted 3rd Party`.
Hence, we are planning to authenticate said contact data requests to {term}`Venue Owner`s in the future.
