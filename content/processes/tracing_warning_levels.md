(process:tracing:warning_levels)=
# Notifying/Warning Users

## Overview

### Warning Levels

User notifications can have different "severity levels". Some of which are informational and others are meant to communicate a "call to action" to the receiving {term}`Guest`.
Depending on the severity level, notifications/warnings are generated automatically by the {term}`Luca Server` or require a manual action by a {term}`Health Department`.
The table provides an overview of the notification/warning levels available in _luca_.

``````{list-table}
:header-rows: 1
:name: tracing_warning_levels

* - Levels
  - Description
  - Event Trigger
* - *#1 - Data Access Notification*
  - **Informs** {term}`Traced Guest`s that their data was accessed by a specific {term}`Health Department`.
  - Once a {term}`Venue Owner` decrypted the outer layer of the {ref}`requested Check-In data <process:tracing:contacts>`, _luca_ automatically notifies all affected {term}`Traced Guest`.
* - *#2 - Potential Risk of Infection*
  - **Informs** all {term}`Guest`s of a venue in a determined time span about their *potential risk of infection*. Asks {term}`Guest`s to behave responsibly, reduce their face-to-face meetings and get tested. The notification contains contact details of the issuing Health Department.
  - {term}`Health Department`s can issue this notification for any venue and time span, even if the {term}`Venue Owner` did not provide access to the {term}`Contact Data` of other Guests.
* - *#3 - Increased Risk of Infection* [^nyi]
  - **Warns** specific {term}`Traced Guest`s that a {term}`Health Department` determined an *increased risk of infection* for them. Asks {term}`Guest`s to behave responsibly, reduce their face-to-face meetings and get tested. The notification contains contact details of the issuing Health Department.
  - {term}`Health Department`s can manually issue this warning to quickly inform large numbers of specific {term}`Traced Guest`s about their increased risk of infection.
* - *#4 - Infection Cluster Warning* [^nyi]
  - **Warns** {term}`Traced Guest`s that one of their {term}`Check-In`s might be associated to *an infection cluster*. Asks {term}`Guest`s to behave responsibly, reduce their face-to-face meetings and get tested.
  - Once more than one {ref}`Check-In tracing request <process:tracing:contacts>` was issued for the same location and time span, _luca_ automatically issues this warning to the affected {term}`Traced Guest`s.
``````

[^nyi]: The trigger for warning levels #3 and #4 are currently under active development. {term}`Guest App`s might not yet receive this warning level at the moment.

## Privacy-Preserving Notifications

To implement the above-described notifications and warnings in a privacy-preserving manner, the {term}`Luca Server` publishes a list of hashed {term}`trace ID`s affected by contact tracing activities of {term}`Health Department`s.
{term}`Guest App`s download this list on a regular basis and check their recently used {term}`trace ID`s (which the app keeps track of locally) against them.

If the {term}`Guest App` finds a match locally, it informs the {term}`Guest` that one of their {term}`Check-In`s was involved in an infection event, whether they are advised to act on the notification and (if applicable) which {term}`Health Department` (with direct contact details) is responsible for the notification.

As the notification lists are publicly available, the utilized hashing mechanism must ensure that no uninvolved party learns the association between {term}`Guest`, {term}`Health Department`, {term}`Check-In`, severity level and venue.

### Notification List Construction

The outlined notification list contains keyed hashes that are constructed as follows:

```
hash = HMAC-SHA256(key=traceID, data=healthDepartmentId + severityLevel).truncate(16)
```

where `traceID` is the ID of the {term}`Check-In` to be warned, `healthDepartmentId` references the {term}`Health Department` responsible for the warning and the `severityLevel` substantiates the notification type. All hashes are truncated to 16 bytes.

The result is a flat list of 16 byte hashes published by the {term}`Luca Server` that is updated whenever new notification hashes are generated.
Each individual hash is removed from the list after a life time of 28 days.
To save bandwidth, the notification list might be paginated based either on time slices and/or a maximum page size.

### Notification List Lookup

Each {term}`Guest App` occasionally fetches the updates of the notification list to detect notification hashes addressed to it.
To do that, the {term}`Guest App` must generate hashes for all combinations of locally stored {term}`Check-In`s, all existing `healthDepartmentIds` and all supported severity levels (currently: 1-4).

The results are individually compared to the latest notification list.
If any combination matches the list, the {term}`Guest App` learns which {term}`Check-In`, {term}`Health Department` and severity level is responsible for the warning and warns the {term}`Guest` accordingly.

The {term}`Guest App` has predefined notification wordings for each severity level.
Hence, it is not supported that {term}`Health Department`s send custom messages to individual users.

## Security Considerations

### Secrecy of Trace ID and Privacy Guarantees

The notification list is public and can be fetched by anyone.
An attacker with access to a specific {term}`trace ID` can therefore perform the necessary hash calculations and lookups to detect and interpret warnings for this specific {term}`trace ID`.

The {term}`trace ID` is a 128bit pseudo-random string that can neither be efficiently guessed nor is it publicly shared by the {term}`Luca Server` or other components in any way.
Despite the {term}`trace ID`s referencing a specific {term}`Check-In` on the {term}`Luca Server`, it does not bear any personal information of the {term}`Guest` that created the {term}`Check-In`.

By design, the {term}`Venue Owner Frontend` and the {term}`Operator App`/{term}`Scanner Frontend` might learn about some or all of the {term}`trace ID`s associated to the venue.
In the very most cases, venue owners will have {ref}`become aware of infection cases in their venues<process:tracing:contacts>` anyway; hence, they do not gain new information from warnings issued for their own venue.

Finally, the {term}`trace ID`s generated via {ref}`static badges<badge:check_in>` are enumerable by anyone with knowledge of the {ref}`Badge's QR code<badge:static_badge_gen:qr_code_content>`.
Hence, in particular, the scanners that {ref}`performed Check-Ins for the static Badge<badge:check_in:considerations>`.
As static Badges obviously won't benefit from dynamic notifications anyway, {term}`Check-In`s with static Badges do not emit any notification list entries.

Given the above discussion, we assume the {term}`trace ID` generated by the {term}`Guest App` as a reasonably private information to use as a secret for the above-described keyed hash.
Without the knowledge of the 128 bit {term}`trace ID` no meaningful information regarding {term}`Guest`s, {term}`Health Department`s or Venues can be retrieved from the public notification list.

### Access Notification List reveals the Number of Traced IDs

The number of hashed {term}`trace ID`s in the above-described notification list allows an estimate on how many {term}`trace ID`s were warned by {term}`Health Department`s across Germany in the last weeks.
Given that many Health Departments participate in the luca system no association to individual Health Departments is possible, though.
