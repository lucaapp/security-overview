(process:health_department_registration)=
# Health Department Registration

_luca_ helps {term}`Health Department`s to trace contact persons and identify infection clusters.
In order to participate in the system Health Departments need to be registered and onboarded first.

## Overview

```{panels}
Participants
^^^
* {term}`Luca Server`
* {term}`Health Department`
* {term}`Health Department Frontend`
* {term}`Health Department Certificate Signing Tool`

---

Assets
^^^
* {term}`Health Department Information`

---

Preconditions
^^^
* the {term}`Health Department` is not onboarded

---

Postconditions
^^^
* the {term}`Health Department` has received their {term}`Health Department Certificate` from a {term}`Trusted 3rd Party` (here: the Bundesdruckerei) (out-of-band)
* an admin user for the {term}`Health Department` has been registered
* the {term}`Health Department Information` is stored on the {term}`Luca Server`
* the Health Department's {term}`HDEKP`'s and {term}`HDSKP`'s public keys are signed using the {term}`Health Department Certificate` and  stored on the {term}`Luca Server`; the private keys are stored locally at the Health Department
* relevant {term}`daily keypair`s have been re-encrypted by an existing Health Department
```

## Secrets

The following {ref}`secrets <secrets>` are involved in this process:

``````{list-table}
:header-rows: 1
:widths: 1 2 2
:name: Health Department Registration Secrets

* - Secret
  - Use / Purpose
  - Location
* - {term}`HDEKP`
  - Encrypt/decrypt the {term}`daily keypair`.
  - The private key is stored locally on the device that runs the {term}`Health Department Frontend`.
    The public key is stored on the {term}`Luca Server`.
* - {term}`HDSKP`
  - Sign the {term}`daily keypair` during {ref}`process:daily_key_rotation`.
  - The private key is stored locally on the device that runs the {term}`Health Department Frontend`.
    The public key is stored on the {term}`Luca Server`.
* - {term}`Health Department Certificate`
  - Authenticate to the {term}`Health Department Frontend` and sign the {term}`HDSKP` and {term}`HDEKP`.
  - Stored locally on devices that run the {term}`Health Department Frontend` or the {term}`Health Department Certificate Signing Tool`.
``````

## Process

### Acquisition of a Health Department Certificate

Health Departments require the {term}`Health Department Certificate` for two purposes:

1. **Log into luca's {term}`Health Department Frontend`** to use _luca_  as an authenticated {term}`Health Department` for contact tracing
2. **Sign the Health Department's {term}`HDSKP` and {term}`HDEKP`** to securely communicate with {term}`Venue Owner`s, other {term}`Health Department`s and individual {term}`Guest`s.

The certificate is issued by a {term}`Trusted 3rd Party`; in this case the Bundesdruckerei.
It is the responsibility of the {term}`Trusted 3rd Party` to issue {term}`Health Department Certificate`s to legitimate and eligible {term}`Health Department`s only and that an effective validation of legitimacy is in place.
_Luca_ is not involved in the issuing of the {term}`Health Department Certificate`.

### Registration of the Health Department

In order to be onboarded to _luca_ the {term}`Health Department` contacts the {term}`Luca Service Operator`.
The Luca Service Operator helps to provide the {term}`Health Department Information` to the {term}`Luca Server` and to set up an admin user account for one of the Health Department's employees.
The admin user can now access the {term}`Health Department Frontend` using the {term}`Health Department Certificate` (TLS client authentication) and the credentials for their user account.

(process:health_department_registration:HDxKP)=
#### Generation and Signing of Health Department Key Pairs

``````{margin} Example: JWT representing a signed HDSKP and the associated meta data
```{code}
{
 "alg": "RS512",
 "typ": "JWT"
}.
{
  // Health Department ID (on the Luca Server)
  "sub": "7f1ffab1-f343-4499-bb89-21b1e961b537",

  // SHA-1 of the issuing Health Department Certificate
  "iss": "b64810d7c66875d6fbc3cd1a77745ac2",

   // Name of the Health Department
  "name": "Gesundheitsamt Beispielfurt",

  // HDSKP public key (base64)
  "key": "BJGBT0vYL53gzK8WoWzg6ub2BIqvYPwquc9EnTYs+ZAabPSxlc9hL2H0M8xWM9oSepl56sTG6HpGAVQ7fChlf54=",

  // Key Type
  "type": "publicHDSKP",

  // Issue date (unix time stamp)
  "iat": 1516239022
}.
<signature bytes>
```
``````

When the admin user logs into the {term}`Health Department Frontend` for the first time the {term}`Health Department Frontend` automatically generates two keypairs [^eckeypairs]: the {term}`HDEKP` (Health Department Encryption Key Pair) and the {term}`HDSKP` (Health Department Signing Key Pair).
These keypairs are used to secure various communications in _luca_, most notably the {ref}`Daily Key Rotation Process<process:daily_key_rotation>`.

After keypair generation, the administrator is requested to download the {term}`Health Department Certificate Signing Tool`, a small desktop application used to sign the public keys of {term}`HDSKP` and {term}`HDEKP` using the {term}`Health Department Certificate`.
The {term}`Health Department Certificate Signing Tool` guides the admin user through the signing process, asks for their consent to associate the freshly generated public keys with the identity of the Health Department, and signs them with the {term}`Health Department Certificate`'s private key.

Once successfully signed, HDSKP and HDEKP are effectively certificates allowing the {term}`Health Department` to securely communicate within the _luca_ system.
All above-mentioned private keys remain local and never leave the {term}`Health Department`'s IT infrastructure.

[^eckeypairs]: Both HDSKP and HDEKP are ECC key pairs on NIST's P-256 curve.

##### Signing of existing Health Department Keypairs

Before the end of July 2021 the above-described HDSKP/HDEKP signing process was not implemented.
Since then, {term}`Health Department` administrators of existing Health Departments are requested to retroactively sign their Health Department keypairs as described above.
Once all {term}`Health Department`s have signed their key pairs, all relevant clients will start {ref}`verifying the full certificate chain <appendix:hdxkp_verification>` to the trust anchor of the {term}`Trusted 3rd Party` where necessary.

### Re-Encryption of the Daily Keypair

In the final step of the onboarding process all recent (epidemiologically relevant) {term}`daily keypair`s need to be re-encrypted for the new {term}`Health Department`.
This is necessary in order for the new Health Department to be able to decrypt existing {term}`daily keypair`s with its {term}`HDEKP`.
The re-encryption process is triggered automatically and carried out by any other Health Department that is currently logged in to the {term}`Health Department Frontend` as follows:
* fetch and verify all Health Departments' {term}`HDEKP` certificates (including the new Health Department's recently created key)
* download all relevant {term}`daily keypair`s
* decrypt them using its own {term}`HDEKP`'s private key
* encrypt them for all other Health Departments' {term}`HDEKP`s
* upload them back to the {term}`Luca Server`

This process is very similar to the {ref}`rotation of the daily keypair<process:daily_key_rotation>`.
Please refer to that chapter for further details.

### Adding Further (Non-Admin) Employees

The admin user can create further user accounts that do not have administrative access in the {term}`Health Department Frontend`.
Like the admin user, those users can authenticate to the Health Department Frontend using their individual credentials and the {term}`Health Department Certificate` and use it for contact tracing.
