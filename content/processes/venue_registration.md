(process:venue_registration)=
# Venue Registration

Professional {term}`Venue Owner`s can register their venue with the _luca_ system via a web application.
The venue can then be managed via a web interface in order to set up individual {term}`Operator App`s and {term}`Scanner Frontend`s and to configure other venue-specific parameters (for example auto checkout behavior).

## Overview

```{panels}
Participants
^^^
* {term}`Luca Server`
* {term}`Venue Owner`
* {term}`Venue Owner Frontend`

---

Assets
^^^
* {term}`Venue Information`

---

Preconditions
^^^
* the venue is not registered

---

Postconditions
^^^
* the {term}`venue keypair` is available locally to the {term}`Venue Owner Frontend`
* the {term}`Venue Information` is stored on the {term}`Luca Server`
```

## Secrets

The following {ref}`secrets <secrets>` are involved in this process:

``````{list-table}
:header-rows: 1
:widths: 1 2 2
:name: Venue Registration Secrets

* - Secret
  - Use / Purpose
  - Location
* - {term}`venue keypair`
  - Encrypt the {term}`contact data reference` of Guests during {ref}`check-in<process:guest_checkin>` and decrypt it during {ref}`process:tracing`.
  - Both the public and private key are stored locally by the {term}`Venue Owner Frontend`.
    The public key is shared with {term}`Scanner Frontend`s when they are set up.
    The private key is shared with {term}`Operator App`s in certain roles.
``````

## Process

To initiate the process the {term}`Venue Owner` registers with their email address and a password.
They enter further information, such as the name of the venue and their contact information in the {term}`Venue Owner Frontend` (see {term}`Venue Information` for the complete list of the data collected).

Subsequently, the Venue Owner Frontend generates the {term}`venue keypair`.
Both the public and private key are stored locally[^pksecret].
The keypair's public key is used to set up new {term}`Scanner Frontend`s, which utilize it to encrypt Guests' {term}`contact data reference` during {ref}`process:guest_checkin`.
The keypair's private key is needed by the Venue Owner Frontend in order to lift this encryption when assisting a {term}`Health Department` in the process of {ref}`process:tracing`.

[^pksecret]: The locally stored private key is encrypted using a symmetric secret that authenticated {term}`Venue Owner`s retrieve from the {term}`Luca Server`. This mitigates the risk of leaking the key, as the Venue Owner's authentication credentials are required to use it.

### Operator App Registration

Optionally, the {term}`Venue Owner` can use the {term}`Venue Owner Frontend` to register one or more {term}`Operator App`s for themselves or their employees.

#### Roles

An Operator App can be registered with one of three roles: _Employee, _Supervisor_ or _Administrator_.
These roles have the following capabilities:

``````{list-table}
:header-rows: 0
:name: Operator App Roles
:widths: 1 5

 * - **Employee**
   - * can scan QR codes presented by the {term}`Guest App` to check-in {term}`Guest`s
 * - **Supervisor**
   - * can scan QR codes presented by the {term}`Guest App` to check-in {term}`Guest`s
     * can view the number of Guests seated at each table
     * can check-out Guests by table
 * - **Administrators**
   - * can scan QR codes presented by the {term}`Guest App` to check-in {term}`Guest`s
     * can view the number of Guests seated at each table
     * can check-out Guests by table
     * can approve {term}`Health Department`s' {ref}`tracing requests<process:tracing:contacts>` to assist in contact tracing
``````

#### Provisioning

In order to perform their scanning tasks, Operator Apps of any role are provisioned with the public key of the {term}`venue keypair`.
Additionally, apps in the _Supervisor_ or _Administrator_ role need to be provisioned with the keypair's private key.
The keys are transferred to the App during registration via a QR code displayed in the {term}`Venue Owner Frontend` and scanned by the {term}`Operator App`.
To mitigate the risk of compromising the key material, for example by taking a photo from the displayed QR code, the private key is protected by a six digit PIN[^pin].
The PIN is displayed in the Venue Owner Frontend and manually typed into the Operator App.

[^pin]: A symmetric secret is derived from the PIN using `scrypt` and a random 32 byte salt. The private key is encrypted with the derived secret using `AES-128-GCM` and a random 12 byte initialization vector.

In addition to the key material, the Operator App is provisioned with a session token which is required for the App to perform certain requests, including fetching the symmetric secret to decrypt the private key[^pksecret].
Venue Owners can delete registered Operator Apps and revoke session tokens at any time in the {term}`Venue Owner Frontend`.

## Security Considerations

(process:venue_registration:considerations)=
### Authenticity of the Venue Keypair's Public Key

As the {term}`Venue Owner` holds no certificate with which they could sign the public key of the {term}`venue keypair` there is no secure way to validate its authenticity when it is used in the check-in process.
This affects both the {ref}`process:guest_checkin` and the {ref}`process:guest_self_checkin`.

It is therefore important that the public key is transmitted to the {term}`Scanner Frontend` on a secure out-of-band channel (specifically, not the {term}`Luca Server`).

Prospectively, this will be implemented by attaching the {term}`venue keypair`'s public key to the fragment component of the link to the {term}`Scanner Frontend`, which is created in the {term}`Venue Owner Frontend`.
For printed QR codes for self Check-In the public key will be part of the QR code.

Note that the impact of this only affects the outer layer of the {term}`contact data reference`'s encryption.
It is still encrypted with the {term}`daily keypair` and thus only accessible for the {term}`Health Department`.

### Sensitivity of the Venue Keypair

The {term}`venue keypair`'s private key must not be lost or made accessible to third parties.
Hence, organizational measures are taken to specifically inform the {term}`Venue Owner` that special care must be taken when dealing with this key.
