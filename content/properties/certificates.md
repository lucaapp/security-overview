---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.12
    jupytext_version: 1.9.1
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell} ipython3
:tags: [remove-input]

import os
import sys
sys.path.insert(0, os.path.abspath('../../lib'))

import plantumlmagic
```

(assets:certificates)=
# Certificates

```{code-cell} ipython3
:tags: [remove-input]

%%plantuml

@startuml

frame "TLS Server Certificate" {

card TLS_Root [
D-TRUST Root Class 3 CA 2 2009
]

card TLS_Intermediate [
D-TRUST SSL Class 3 CA 1 2009
]

card TLS_Leaf [
TLS Server Certificate
----
host: *.luca-app.de
]

TLS_Root --> TLS_Intermediate : issues
TLS_Intermediate --> TLS_Leaf : issues

}

frame "TLS Client Certificate" {

card mTLS_Root [
D-TRUST Limited Basic Root CA 1 2019
]

card mTLS_Intermediate [
D-TRUST Limited Basic CA 1-2 2019
]

card mTLS_Leaf [
Health Department Certificate
----
CN: "x.xx.x.xx.luca", OU: "LUCA", O: "Bundesdruckerei"
key usages: digital signature, TLS Client authentication
]

mTLS_Root --> mTLS_Intermediate : issues
mTLS_Intermediate --> mTLS_Leaf : issues

}

frame "Health Department Certificates" {

card HDSKP [
Health Department Signing Key Pair
----
sub: "9ca232a0-5931-4085-935a-c7f1b859dd84"
name: "Gesundheitsamt Beispielfurt"
]

card HDEKP [
Health Department Encryption Key Pair
----
sub: "9ca232a0-5931-4085-935a-c7f1b859dd84"
name: "Gesundheitsamt Beispielfurt"
]

}

card Badge_Keypair [
luca Badge Keypair
]


card Daily_Key [
luca Daily Key
]

cloud encrypted {
card Badge_Keypair_Encrypted [
  luca Badge Keypair
]
card Daily_Key_Encrypted [
  luca Daily Key
]
}

mTLS_Leaf --> HDSKP : signs
mTLS_Leaf --> HDEKP : signs
HDSKP --> Daily_Key : signs
HDSKP --> Badge_Keypair : signs
HDEKP -->> Daily_Key_Encrypted  : encrypts
Daily_Key ~~  Daily_Key_Encrypted
HDEKP -->> Badge_Keypair_Encrypted  : encrypts
Badge_Keypair ~~  Badge_Keypair_Encrypted

@enduml
```

```{glossary}

TLS Server Certificate

    This is the {term}`Luca Server`'s TLS server certificate that is issued by D-Trust for the \*.luca-app.de domain.

Health Department Certificate

    A certificate that identifies a {term}`Health Department`.
    It is used both for mTLS-based authentication to the {term}`Health Department Frontend` and signing of the health department's {term}`HDSKP` and {term}`HDEKP` certificates.

    This certificate is created and maintained by an external, trusted {term}`Certificate Authority`.

HDEKP

    The "Health Department Encryption Keypair" is used to encrypt messages to a specific {term}`Health Department` (e.g. the {term}`daily keypair`'s private key).
    Each Health Department has their own HDEKP.

    The public key of this keypair is signed using the respective {term}`Health Department Certificate` and stored on the {term}`Luca Server`.
    The private key is kept locally at the Health Department.

HDSKP

    The "Health Department Signing Keypair" is used to authenticate messages issues by a specific {term}`Health Department`.
    For instance, the {term}`daily keypair` is signed using the issuing Health Department's HDSKP.
    
    Each Health Department has their own HDSKP which is signed using the respective {term}`Health Department Certificate`.

daily keypair

    The keypair whose public key is used by the {term}`Guest App` to encrypt the secret part of the {term}`Check-In` data.
    Its private key is used by a {term}`Health Department` during the process of {ref}`Contact Tracing<process:tracing>`.

    The keypair's public key is signed using the {term}`HDSKP` and stored on the {term}`Luca Server`.
    Its private key is encrypted for each registered Health Department's {term}`HDEKP`.
    The encrypted private keys are stored on the {term}`Luca Server`.

    The daily keypair's life cycle and usage is detailed in the chapter {ref}`process:daily_key`.


badge keypair

    The keypair that encrypts {term}`contact data reference`s for static {term}`Badge`s.
    It is technically equivalent to the {term}`daily keypair` but is used exclusively by a {term}`Trusted 3rd Party` during {ref}`the generation of static Badges <badge:static_badge_gen>`.

    Its private key is owned by the Health Department and is used to decrypt {term}`Check-In`s created using a static Badge.
    The badge keypair can be issued by any Health Department and is signed by the respective Health Department's {term}`HDSKP`.
```

